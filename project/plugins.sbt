addSbtPlugin("io.spray"      % "sbt-revolver"  % "0.8.0")
addSbtPlugin("org.scala-js"  % "sbt-scalajs"   % "0.6.8")
addSbtPlugin("com.scalakata" % "sbt-scalakata" % "1.1.1")