package ch.epfl.scaladex

trait Api {
  def hello(name: String): String
}